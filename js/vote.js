(function ($) {
  Drupal.ajax.prototype.commands.vot = function (ajax, response, status) {
    $(response.selector).html(response.text);
    Drupal.behaviors.voteSlider.calculate();
  };
  Drupal.behaviors.voteSlider = {
    attach: function (context, settings) {
      this.calculate();
    },
    calculate: function () {
      if ($('.no-procent').length) {
        console.log('calculate');
        $noSlider = $('.no-procent');
        $yes = $('.yes').text();
        $no = $('.no').text();
        $total = parseFloat($yes) + parseFloat($no);
        $width = ($no / $total) * 100 + '%';
        $noSlider.css('width', $width);
      }
    }
  };
})(jQuery);